$(function(){

    // jQuery methods go here...
	$("#myInput").on("keyup", function(e) {
		q = e.currentTarget.value.toLowerCase()
        console.log(q)
        
        let urlItem = "https://www.googleapis.com/books/v1/volumes?q=" + q

        $.ajax({
            async : true,
            type : 'GET',
            url: urlItem,
            datatype: 'json',
            
            success: function(data){
            $('tbody').empty();
            var result;
            
            for(var i = 0; i < data.items.length; i++) {
                
                result += "<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td></tr>";
            }
            $('tbody').append(result);
            console.log("testDone");
            }
        });
    });
});
