from django.test import TestCase,Client
from django.urls import resolve
from . import views

class app_unit_test(TestCase):
    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_app_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_app_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
